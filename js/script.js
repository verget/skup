
$(function() {
	$('body').on('click','.to_cart', function() {
		var item_id = this.parentNode.id;
		$.getJSON('/to_cart/'+item_id,  function(data){
			$('#cart_count').text(data);
            $('#cart').popover('show');
            setTimeout(function() {$('#cart').popover('hide')}, 2000);
		});
		return true;
	});
	$('body').on('click','.to_cart_modal', function() {
		var item_id = $('#modal_container').attr('name');
		$.getJSON('/to_cart/'+item_id,  function(data){
			$('#cart_count').text(data);
			$('#cart').popover('show');
	        setTimeout(function() {$('#cart').popover('hide')}, 2000);
		});
		return true;
	});
	
	
	$('#cart').on('click', function() {
	    $('.cart_list').text('');
	    $('.cart_summ').text('');
	    var summ = 0;
        $.getJSON('/get_cart/',  function(data){
            $.each(data, function(item, value) {
                summ = summ + parseInt(value.price);
                $('.cart_list').append("<tr><td class = 'title'>"+value.title + 
                        "</td><td class = 'price'>" + value.price+"р. </tr>");
            });
            $('.cart_summ').text(summ);
        });
        return true;
    });
	
	$('.set_order').on('click', function() {
        var name = $('#inputName').val();
        var address = $('#inputAddress').val();
        if(!name || !address) {
            alert ("Не заполнены поля!")
        }else{
    	    $.ajax({
    	        url: '/new_order',
    	        type: 'POST',
    	        dataType: 'json', 
    	        data: { 'name':name, 'address': address },
    	        success: function(data){ alert('Спасибо за покупку! Номер Вашего заказа #'+data); }
    	    });
    	    $('.cart_list').text('');
            $('.cart_summ').text('');
            $('#cart_count').text(0);
        }
        return true;
	});
	$('#center-block-btn').on('click', function(){
		$('#list').text('');
		$('.ul_pagination').hide();
		$('#loadingDiv').show();
		var arr = [];
		var cat = $(this).attr('name');
		var type = $('#id_price_type').val();
		var price_from = $('#id_price_from').val();
		var price_to = $('#id_price_to').val();
		$(".checkbox_filter:checked").each(function(){ 
			arr.push($(this).val());
		});
		$('#list').load( "/render_items #list",{
				'params': arr, 'category_name' : cat, 'price_type': type, 
				'price_from': price_from, 'price_to': price_to}, function(){
					$('#loadingDiv').hide();
				} );
	});
	
//	var $loading = $('#loadingDiv').hide();
//	$(document)
//	  .ajaxStart(function () {
//	    $loading.show();
//	  })
//	  .ajaxStop(function () {
//	    $loading.hide();
//	  });
	
	$('body').on('click', '.open_more', function(){
		var item_id = this.parentNode.id;
		$('#modal_container').load( "/object", {'id': item_id} );
		$('#modal_container').attr('name', item_id);
	})
	$('#submit_form').on('click', function(){
	    if ($('#object_title').val() == ''){
	        alert("Заполните заголовок!");
	        return false
	    }
	    if ($('#category_id').val()=='0'){
	        alert("Выберите категорию!");
	        return false
	    }
        $('#object-edit-form').submit();
	    return false;
	});
	
	
    $(".bootstrap_validate_form").validate({
        rules: {
            username:{
                required: true,
                minlength: 4,
                maxlength: 16,
                pattern: /^[A-Za-z0-9_]{4,16}$/
            },
            password:{
                required: true,
                minlength: 4,
                maxlength: 16
            },
            repeat_password:{
                equalTo: '#password'
            },
            // compound rule
            userrole: {
                required: true
            }
        }
    });
    $(function () {
        $('[data-toggle="popover"]').popover()
      });

  });
function uploadProcess(mode){
    if( mode )
        $('#upload-btn').attr('disabled', 'disabled')
            .find('span').removeClass('glyphicon-save').addClass('glyphicon glyphicon-refresh icon-span');
    else
        $('#upload-btn').removeAttr('disabled')
            .find('span').removeClass('glyphicon-refresh icon-span').addClass('glyphicon-save');
}
function removeImage(img_id){
    $.getJSON('/admin/removeimage/'+img_id, function(req){
        if( req.result ){
            $('#ob-image-'+img_id).fadeOut('fast', function(){ $(this).remove() });
        }
    });
    return false;
}
