<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/


$route['default_controller'] = 'ads/index';
//$route['p/(:any)'] = 'ads/get_category/$1';
// $route['p'] = 'ads/get_category/0';

$route['category/(:any)/p/(:any)'] = 'ads/get_category/$1/$2';
$route['category/(:any)'] = 'ads/get_category/$1';
// $route['category/(:any)/p'] = 'ads/get_category/$1/0';
$route['object'] = 'ads/object';

$route['page/(:any)'] = 'pages/view/$1';
$route['page'] = 'pages';

$route['get_type/(:any)'] = 'ads/get_type/$1';

$route['to_cart/(:any)'] = 'ads/to_cart/$1';
$route['get_cart'] = 'ads/get_cart';
$route['new_order'] = 'ads/new_order';

$route['render_items'] = 'ads/render_items';

// -------- ADMIN SECTION -----------
$route['admin/403'] = 'admin/page403/index';
$route['admin'] = 'admin/ads/index';
$route['admin/p'] = 'admin/ads/index/0';
$route['admin/p/(:any)'] = 'admin/ads/index/$1';
$route['admin/new'] = 'admin/ads/create';
$route['admin/edit/(:any)'] = 'admin/ads/edit/$1';
$route['admin/edit/(:any)/save'] = 'admin/ads/edit/$1/save';
$route['admin/upload/(:any)'] = 'admin/ads/uploadimages/$1';
$route['admin/removeimage/(:any)'] = 'admin/ads/removeimage/$1';
$route['admin/action'] = 'admin/ads/action';

$route['admin/banners'] = 'admin/banners/index';
$route['admin/banners/edit/(:any)'] = 'admin/banners/edit/$1';
$route['admin/banners/edit/(:any)/save'] = 'admin/banners/edit/$1/save';
$route['admin/banners/action'] = 'admin/banners/action';
$route['admin/banners/new'] = 'admin/banners/create';
$route['admin/banners/upload/(:any)'] = 'admin/banners/upload_image/$1';
$route['admin/banners/remove_image/(:any)'] = 'admin/banners/remove_image/$1';



$route['admin/about'] = 'admin/dashboard/about';
$route['admin/about/save'] = 'admin/dashboard/about/save';

$route['admin/config_page'] = 'admin/dashboard/config_page';
$route['admin/config_page/save'] = 'admin/dashboard/config_page/save';

$route['admin/login'] = 'admin/login/index';
$route['admin/login/list'] = 'admin/login/users_list';
$route['admin/login/create'] = 'admin/login/create';
$route['admin/login/edit/(:any)'] = 'admin/login/edit/$1';
$route['admin/login/delete/(:any)'] = 'admin/login/delete/$1';
$route['admin/logout'] = 'admin/login/out';

/* End of file routes.php */
/* Location: ./application/config/routes.php */
