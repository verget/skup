<?php
include_once APPPATH . 'controllers/prototype.php';
class Ads extends Prototype {
    private $_search = [];
    private $_searchFields = [];
    
    public function __construct(){
        parent::__construct();
        $this->load->model( 'ads_model' );
//         $this->_searchFields = [
//                         'avail_category' => $this->ads_model->getAvailCategory(0, 'Любая' ),
//         ];
//         $this->_search = Ads_model::checkSearchFields( $this->input->get( 'search' ) );
    }
    public function index(){
    
        $this->load->model( 'slides_model' );
        $this->load->view( 'templates/header', [
                        'title' => '',
                        'config' => $this->config_model->getConfig(),
        ] );
    
        $this->load->view( 'ads/category_plate');
        $this->load->view( 'templates/slider', [
        								'slides' => $this->slides_model->get_slides(),
        ]);
        $this->load->view( 'templates/footer' );
    }
    
    public function view($description = "index"){ 
        $data = [
                    'ads_item' => $this->ads_model->get_ads( $description ),
                    'search' => $this->_search 
        ];
        if(empty( $data['ads_item'] )){
            show_404();
        }
        
        $data['title'] = $data['ads_item']['title'];
        $this->load->view( 'templates/header', $data );
        $this->load->view( 'ads/view_ads', $data );
        $this->load->view( 'templates/footer' );
    }
    
    
    public function get_category($category_name, $pageIn = 0){
        
        $this->load->model( 'config_model' );
        $this->load->model( 'slides_model' );
        $config = array(
                        'base_url' => '/category/'.$category_name.'/p',
                        'per_page' => 18, 
                        'uri_segment' => 4,
                        'total_rows' => 0,
                        'first_url' => '/category/'.$category_name.'/p/1'
        );
        $page = ($pageIn > 1) ? $pageIn : 0;
        $category = $this->ads_model->get_category_by_name($category_name);
        $parrent_category = $this->ads_model->get_category_by_id($category->parrent_id);
        
        $this->_search['category_id'] = $category->id;
        
        if(!$category_list = $this->ads_model->get_child_categories($category->id))
        	$category_list = $this->ads_model->get_child_categories($category->parrent_id);
        $config["total_rows"] = $this->ads_model->fetch_ads( $this->_search, $config["per_page"], $page, true);
        
        if(count( $_GET ) > 0)
            $config['suffix'] = '?' . http_build_query( $_GET, '', "&" );
        $config['first_url'] = $config['base_url'] . '?' . http_build_query( $_GET );
        
        $this->pagination->initialize( $config );
        
        $this->load->view( 'templates/header', [
                        'title' => '',
                        'config' => $this->config_model->getConfig(),
        ] );
        $this->load->view ('templates/breadcrump', [
						'category'=> $category,
						'parrent_category'=> $parrent_category
        ]);
        $this->load->view( 'ads/item_list', [
                        'ads' => $this->ads_model->fetch_ads( $this->_search, $config["per_page"], $page, false),
    					'category'=> $category,
    					'child_categories' => $category_list,
                        'links' => $this->pagination->create_links(),
        ] );
        $this->load->view( 'templates/footer' );
    }
    
    public function render_items(){
//     	$config = array(
//     			'base_url' => '/p/',
//     			'per_page' => 1,
//     			'uri_segment' => 2,
//     			'total_rows' => 0,
//     			'first_url' => '/'
//     	);
     	$page = 0;
    	
    	$category = $this->ads_model->get_category_by_name($_POST['category_name']);
    	$this->_search['category_id'] = $category->id;
    	
    	if(!empty($_POST['params'])){
	    	$array = $_POST['params'];
	    	foreach ($array as $param){
	    		$this->_search[$param] = '1';
	    	}
    	}
    	if(!empty($_POST['price_from']) || !empty($_POST['price_to'])){
    		$this->_search['price_type'] = $_POST['price_type'];
    		$this->_search['price_from'] = $_POST['price_from'];
    		$this->_search['price_to'] = $_POST['price_to'];
    	}
    	$this->load->view( 'ads/item_list', [
    			'ads' => $this->ads_model->fetch_ads( $this->_search),
    			//'links' => $this->pagination->create_links(),
    			] );
    }

    public function object(){
    		$ads_id = ($_POST['id']) ? ($_POST['id']) : 0;
        $ads = $this->ads_model->fetch_one_ads( $ads_id );
        if(empty( $ads ))
        	show_404();
        $this->load->view( 'ads/result_ads', [
            'ads_item' => $ads 
        ] );
    }
    
    public function get_type($category_id){
       $array = $this->ads_model->getAvailTypes( 0, 'Любой', (int)$category_id );
       $a = array();
       if(!empty($array)){
           foreach ($array as $type){
              $a[$type->type_id] = $type->type_title;
           }
       }      
       echo json_encode($a);
    }

    public function to_cart($item_id){
        session_start();
        if(!isset($_SESSION['cart']))
            $_SESSION['cart'] = array();
    	$cart = $_SESSION['cart'];
    	array_push($cart, $item_id);
    	$_SESSION['cart'] = $cart;
    	echo count($cart);
    }
    
    public function get_cart(){
       session_start();
       $cart = $_SESSION['cart'];
       $items = $this->ads_model->getItems($cart);
       $a = array();
       $i = 1;
       if(!empty($items)){
           foreach ($items as $item){
//                $k = 0;
//                foreach($items as $key => $it){
//                    if($item->id == $it->id){
//                        $k++;
//                        if($k > 1) unset($items[$key]);
//                    }
//                }
//                $item->count = $k;
               $a[$i] = $item;
               $i++;
           }
       }      
       echo json_encode($a);
    }
    
    public function new_order(){
        session_start();
        $name = $_POST['name'];
        $address = $_POST['address'];
        $items = $_SESSION['cart'];
        $num = $this->ads_model->setOrder($name, $address, $items);
        unset($_SESSION['cart']);
        echo $num;
    }
}