<script type="text/javascript">
	$(document).ready(function() {
		$(".fancybox").fancybox();
	});
</script>
<?php $images = $ads_item->images;?>
<?php 
$img_path = '/images/';
if( empty($images) )
	$first_img = '/img/none.jpg';
else
	$first_img = $img_path . $images[0]->image_url;
?>
<div class="conteiner">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <center><?php echo $ads_item->title ?></center>
    <br />
    <div class="row">
        <div class="col-xs-offset-1 col-xs-5 ">
        	<a class="fancybox" rel="group" href="<?php echo $first_img?>">
        	  <img src="<?php echo $first_img?>" class="img-responsive first-img" /> 
        	</a>
        </div>
        <div class="col-xs-6">
            <?php if( !empty($images) ):?>
             <?php array_shift($images) ;?>
            <?php foreach( $images as $image ):?>
                <div class="col-xs-6">
                    <a class="fancybox img-thumbnail" style = "margin: 1px" 
                    		rel="group" href="<?php echo $img_path . $image->image_url?>">
                    		<img src="<?php echo $img_path . $image->image_url?>" class = "img-responsive"/>
                    </a>
                </div>
            <?php endforeach;?>
            <?php endif;?>
    	</div>
	</div>
	<br />
	<?php if ($ads_item->in_stock):?>
    	<div class="row">
    	   <div class="col-xs-offset-1 col-xs-5">
    	   </div>
    	   <div class="col-xs-6">
    	       <?php echo "<p class='text-success'>Сейчас в наличии.</p>"?>
    	   </div>
    	</div>
	<?php endif;?>
	<?php if ($ads_item->day_price):?>
    	<div class="row">
    	   <div class="col-xs-offset-1 col-xs-5">
    	       <span> Цена за день:</span>
    	   </div>
    	   <div class="col-xs-6">
    	       <?php echo "<p class='text-danger prices'>".$ads_item->day_price." руб.</p>"?>
    	   </div>
    	</div>
	<?php endif;?>
	<?php if ($ads_item->week_price):?>
    	<div class="row">
    	   <div class="col-xs-offset-1 col-xs-5">
    	       <span> Цена за неделю:</span>
    	   </div>
    	   <div class="col-xs-6">
    	       <?php echo "<p class='text-danger prices'>".$ads_item->week_price." руб.</p>"?>
    	   </div>
    	</div>
	<?php endif;?>
	<?php if ($ads_item->month_price):?>
    	<div class="row">
    	   <div class="col-xs-offset-1 col-xs-5">
    	       <span> Цена за месяц:</span>
    	   </div>
    	   <div class="col-xs-6">
    	       <?php echo "<p class='text-danger prices'>".$ads_item->month_price." руб.</p>"?>
    	   </div>
    	</div>
	<?php endif;?>
	<?php if ($ads_item->pledge):?>
    	<div class="row">
    	   <div class="col-xs-offset-1 col-xs-5">
    	       <span> Залог:</span>
    	   </div>
    	   <div class="col-xs-6">
    	       <?php echo "<p>".$ads_item->pledge." руб.</p>"?>
    	   </div>
    	</div>
	<?php endif;?>
	<?php if ($ads_item->min_time):?>
    	<div class="row">
    	   <div class="col-xs-offset-1 col-xs-5">
    	       <span> Минимальный срок аренды:</span>
    	   </div>
    	   <div class="col-xs-6">
    	       <?php echo "<p>".$ads_item->min_time."</p>"?>
    	   </div>
    	</div>
	<?php endif;?>
	<?php if ($ads_item->description):?>
    	<div class="row">
    	   <div class="col-xs-offset-1 col-xs-5">
    	       <span> Описание:</span>
    	   </div>
    	   <div class="col-xs-6">
    	       <?php echo "<p>".$ads_item->description."</p>"?>
    	   </div>
    	</div>
	<?php endif;?>

</div>
                            