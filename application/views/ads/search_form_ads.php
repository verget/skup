<div class="container">
	<div class="row">
	 	<div class="col-xs-12"> 
		    <div data-toggle="buttons" class="btn-group">
	        <label class="btn btn-default filter_btn"> Новинки
	                <input type="checkbox" value="new" id="new_filter" class="checkbox_filter">
	    		</label>
	    		<label class="btn btn-default filter_btn"> В наличии
	                <input type="checkbox" value="in_stock" id="stock_filter" class="checkbox_filter">
	    		</label>
	    		<label class="btn btn-default filter_btn"> Акция
	                <input type="checkbox" value="sale" id="sale_filter" class="checkbox_filter">
	    		</label>
	    	</div>
		</div>
	</div>
</div>
<hr>