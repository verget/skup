<div class="modal fade" id="item_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
      <div class="modal-dialog">
        <div class="modal-content">

    	      <div class="modal-body" id="modal_container">
    	      
    	      </div>
            <div class="modal-footer">
    	        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
    	        <button type="button" class="btn btn-primary to_cart_modal" data-dismiss="modal">В корзину</button>
            </div>
        </div>
      </div>
</div>

<div class="container search-page">
    <h4><?php echo $category->category_title;?></h4>
    <hr class="thick">
    <div class="col-sm-9 no-left-padding">
            <div id='loadingDiv' style="display: none;">
            	<img src='/img/loading.gif' class="img-responsive"/>
						</div>
            <div id="list" class="tab-pane active">
						  
                <ul class="media-list list-unstyled ads-list">
                <?php if( !empty($ads) ):?>
                    <?php foreach ($ads as $ads_item):?>
                        <li class="media discount-container">
                            <div class="hidden-xs hidden-sm media-right">
                                <a class="pull-right arrow" href="/"> </a>
                            </div>
                            <a class="pull-left thumb" title="<?php echo $ads_item->title?>" href="/"> 
                                <?php  if($ads_item->image_url):?>
                        			<?php echo ("<img class='center-block img-responsive item_img' src='/images/$ads_item->image_url' title='$ads_item->title' alt='$ads_item->title'>");?>
                        		<?php else: echo ("<img src='/img/none.jpg' class='center-block img-responsive item_img' title='$ads_item->title' alt='$ads_item->title'>"); ?>
                    			<?php endif;?>
                            </a>
            			    <div class="media-body">
                            <h6 class="media-heading"> 
                                <a data-toggle="modal" data-target="#item_modal" class="open_more">
                                    <?php echo $ads_item->title ?>
                                </a>
                            </h6>
                            <p class="description"><?php echo $ads_item->description?></p>
                            <p class="text-danger prices"> 
                            	<?php if($ads_item->day_price) echo $ads_item->day_price." руб. за день, "?>
                            	<?php if($ads_item->week_price) echo $ads_item->week_price." руб. за неделю, "?>
                            	<?php if($ads_item->month_price) echo $ads_item->month_price." руб. за месяц"?> 
                            </p>
                            <div id="<?php echo $ads_item->item_id?>">
                    			<button class="btn btn-sm btn-default to_cart" >В корзину</button>
                    			<button type="button" data-toggle="modal" data-target="#item_modal" class="btn btn-sm btn-default open_more">Подробнее</button>
                    	    </div>
                        </div>
                    </li>
                    <?php endforeach ?>
                <?php else:?>
                <h4>По Вашему запросу ничего не найдено</h4>
                <?php endif;?>      
            </div>
    </div>
    <div class="col-sm-3 hidden-xs pull-right">
        <div class="row">
            <ul class="list-unstyled categories">
            	 <?php if( !empty($child_categories) ):?>
                    <?php foreach ($child_categories as $cat):?>
                    	<?php if ($category->category_title == $cat['category_title']):?>
                    		<li>
                    			<span class="selected">
                    				<?php echo $cat['category_title'];?>
                    			</span>
                    		</li>
                    	<?php else:?>
				                <li>
			                    <a href="/category/<?php echo $cat['category_name'];?>">
		                        <?php echo $cat['category_title'];?>
			                    </a>
				                </li>
				              <?php endif;?> 
                		<?php endforeach ?>
               <?php endif;?>  
            </ul>
        </div>
        <div class="row">
            <form method="get" class="smart-search form-horizontal" name="filter_form">
                <div class="form-group">
                    <label for="id_price_type" class="col-xs-3 control-label">Цена</label>
                    <div class="col-xs-8">
                        <select name="price_type" id="id_price_type" class="bordered">
                            <option selected="selected" value="day_price">За день</option>
                            <option value="week_price">За неделю</option>
                            <option value="month_price">За месяц</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    
                    <label for="id_price_from" class="col-xs-3 control-label">От&nbsp;</label>

                    <div class="col-xs-9">
                        <input type="number" name="price_from" maxlength="8" id="id_price_from" class="bordered price">
                        <span class="hint">&nbsp;руб.</span>
                    </div>
                </div>
                <div class="form-group">
                    
                    <label for="id_price_to" class="col-xs-3 control-label">До&nbsp;</label>

                    <div class="col-xs-9">
                        <input type="number" name="price_to" maxlength="8" id="id_price_to" class="bordered price">
                        <span class="hint">&nbsp;руб.</span>
                    </div>
                </div>
                <div class="form-group filter_btns">
                    <div data-toggle="buttons" class="btn-group">
        	        <label class="btn btn-default filter_btn btn-sm"> Новинки
        	                <input type="checkbox" value="new" id="new_filter" class="checkbox_filter">
        	    		</label>
        	    		<label class="btn btn-default filter_btn btn-sm"> В наличии
        	                <input type="checkbox" value="in_stock" id="stock_filter" class="checkbox_filter">
        	    		</label>
        	    		<label class="btn btn-default filter_btn btn-sm"> Акция
        	                <input type="checkbox" value="sale" id="sale_filter" class="checkbox_filter">
        	    		</label>
        	    	</div>
                </div>
                <div class="form-group">
                    <button type="button" name="<?php echo $category->category_name;?>" class="btn btn-default center-block" id="center-block-btn">Найти</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container">
    <hr class="thick no-bottom-margin">
    <div class="pull-right">
        <div class="ul_pagination" style = "position: relative; clear: both;">
        <p><?php echo $links; ?></p>
    </div>
    </div>
</div>    