<h2>Редактирование объекта</h2>

<form role="form" action="/admin/edit/<?php echo $ad->item_id?>/save" method="post" id="object-edit-form">

<div class="container-fluid">
	<div class="row">
		<div class="form-group col-xs-12">
			<label for="object_title">Заголовок</label>
			<input type="text" class="form-control" id="object_title" 
				name="object[title]" placeholder="Заголовок" value="<?php echo $ad->title?>" />
		</div>
	</div>
	<div class="row">
		<div class="form-group col-xs-4">
			<label for="category[category_id]">Категория</label>
			<select class="form-control" id="category_id" name="category[category_id]" >
			<?php foreach ($category_list as $item):?>
				<option value="<?php echo $item->id?>"
					<?php echo ($ad->category_id == $item->id)?'selected="selected"':''?>
					><?php echo $item->category_title?></option>
			<?php endforeach;?>
			</select>
		</div>
		<div class="form-group col-xs-4">
			<label for="object[article]">Артикул</label>
			<input type="text" class="form-control" id="object[article]" 
				name="object[article]" placeholder="Артикул" value="<?php echo $ad->article?>" />
		</div>
		<div class="form-group col-xs-4">
				<label for="object[in_stock]">В наличии</label>
				<select id="object[in_stock]" name="object[in_stock]">
				    <option value="1" <?php if($ad->in_stock == "1") echo "selected";?>>Да</option>
				    <option value="0" <?php if(!$ad->in_stock) echo "selected='selected'";?>>Нет</option>
				</select>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-xs-4">
			<label for="prices[day_price]">Цена за день</label>
			<input type="text" class="form-control" id="prices[day_price]" 
				name="prices[day_price]" placeholder="Цена за день" value="<?php echo $ad->day_price?>" />
		</div>
		<div class="form-group col-xs-4">
			<label for="prices[week_price]">Цена за неделю</label>
			<input type="text" class="form-control" id="prices[week_price]" 
				name="prices[week_price]" placeholder="Цена за неделю" value="<?php echo $ad->week_price?>" />
		</div>
		<div class="form-group col-xs-4">
			<label for="prices[month_price]">Цена за месяц</label>
			<input type="text" class="form-control" id="prices[month_price]" 
				name="prices[month_price]" placeholder="Цена за месяц" value="<?php echo $ad->month_price?>" />
		</div>
	</div>
	<div class="row">
	   <div class="form-group col-xs-4">
			<label for="prices[pledge]">Залог</label>
			<input type="text" class="form-control" id="prices[pledge]" 
				name="prices[pledge]" placeholder="Залог" value="<?php echo $ad->pledge?>" />
		</div>
		<div class="form-group col-xs-4">
			<label for="object[min_time]">Мин. срок аренды</label>
			<select id="object[min_time]" name="object[min_time]">
			 <option value="0"> Любой </option>
			 <option value="1" <?php if($ad->min_time == "1") echo 'selected="selected"';?>>День</option>
			 <option value="2" <?php if($ad->min_time == "2") echo 'selected="selected"';?>>Неделя</option>
			 <option value="3" <?php if($ad->min_time == "3") echo 'selected="selected"';?>>Месяц</option>
			 <option value="4" <?php if($ad->min_time == "4") echo 'selected="selected"';?>>Год</option>
			</select>
		</div>
	</div>
	
	<div class="row">
		<div class="form-group col-md-12">
			<label for="object[description]">Описание</label>
			<textarea name="object[description]" id="object-description" class="form-control" rows="4"><?php echo $ad->description?></textarea>
		</div>
	</div>
	<div class="row">
        <div class="form-group col-md-12">
            <label for="object[meta_desc]">Описание (meta тег)</label>
            <textarea name="object[meta_desc]" id="object[meta_desc]" class="form-control"
                rows="1"><?php echo $ad->meta_desc?></textarea>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-12">
            <label for="object[meta_keywords]">Ключевые слова (meta тег)</label>
            <textarea name="object[meta_keywords]" id="object[meta_keywords]" class="form-control"
                rows="1"><?php echo $ad->meta_keywords?></textarea>
        </div>
    </div>
</div>

</form>

<?php if( $ad->item_id > 0 ):?>
<br />
<div id="uploader">
    <h4>Изображения</h4>

	<form action="/admin/upload/<?php echo $ad->item_id?>" method="post" enctype="multipart/form-data">
		<input type="file" multiple name="photos[]" style="display: inline-block;" /> &nbsp;&nbsp;
		<button type="submit" class="btn" id="upload-btn">
			<span class="glyphicon glyphicon-save"></span> Загрузить
		</button>
	</form>

	<div id="object-image-list">
	<?php if( !empty($ad->images) ):?>
	<?php foreach ($ad->images as $image):?>
		<div class="image" id="ob-image-<?php echo $image->id?>" data-id="<?php echo $image->id?>">
			<img src="/images/<?php echo $image->image_url?>" class="img-thumbnail" />
			<a href="#" onclick="return removeImage(<?php echo $image->id?>);">
				<span class="glyphicon glyphicon-trash"></span> Удалить
			</a>
		</div>
	<?php endforeach;?>
	<?php endif;?>
	</div>
</div>
<?php else:?>
<div class="alert alert-warning" role="alert">
    <strong>Внимание!</strong> Сначала сохраните объект, что бы можно было загружать изображения
</div>
<?php endif;?>
<br />

<button type="button" class="btn btn-success" id="submit_form" >Сохранить</button>

<a href="/admin?<?php echo $search_query?>" class="btn btn-info">Назад</a>

<br />
<br />
