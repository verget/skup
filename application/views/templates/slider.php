<div class="container slider">
	<div class = "row">
		<div class = "col-xs-offset-1 col-xs-10">
	<!-- Слайдер -->
		<?php if (!empty($slides)): ?>
		<?php $count = $slides; ?>			
		 <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="8000">
		  <!-- Индикаторы -->
		  <div class="col-xs-12">
		  <ol class="carousel-indicators">
		    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		    <?php $i = 1;?>
		    <?php unset($count[0]);?>
		    <?php foreach ($count as $c): ?>
		    	<li data-target="#carousel-example-generic" data-slide-to=<?php echo $i?>></li>
		    	<?php $i = $i+1;?>
		    <?php endforeach; ?>
		  </ol>
		  <!-- Содержимое слайдера (Обёртка для слайдов)-->
		  <div class="carousel-inner">
		    <div class="item active">	 
		      <img src="/images/slides/<?php echo $slides[0]->image_url?>" alt="..." class="slide_img"> 
		      <div class="carousel-caption"></div>
		    </div>
		    <?php unset($slides[0]);?>
		    <?php foreach ($slides as $slide): ?>
			    <div class="item">
			      <img src="/images/slides/<?php echo $slide->image_url?>" alt="..." class="slide_img">
			      <div class="carousel-caption"></div>
			    </div>
		    <?php endforeach; ?>
		  </div>
		  </div>
		  <!-- Контролы -->
		  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
		    <span class="glyphicon glyphicon-chevron-left"></span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
		    <span class="glyphicon glyphicon-chevron-right"></span>
		  </a>
		</div>
	<?php endif;?>
		</div>
	</div>
</div>