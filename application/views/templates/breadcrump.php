<div class="breadcrumb">
	<div class="container">
        <ol class="breadcrumb">
            <li><a title="Главная страница Скупердяй" href="/">Главная страница</a></li>
            <?php if($parrent_category):?>
            	<li><a title="<?php echo $parrent_category->category_title?>" 
            		href="/category/<?php echo $parrent_category->category_name;?>">
            			<?php echo $parrent_category->category_title ;?></a></li>
            <?php endif;?>
             <li class="active"><?php echo $category->category_title ;?> </span></li>
        </ol>
	</div>
</div>