﻿<?php $conf = $this->config_model->getConfig();?>

<footer class="main-footer">
	<div class="container">
 		<div class="row hidden-xs">
			<div class="col-xs-offset-1 col-xs-2">
				<ul class="links list-unstyled">
					<li><a>Спорт</a></li>
					<li><a>Для детей</a></li>
					<li><a>Для свадеб</a></li>
				</ul>
			</div>
			<div class="col-xs-offset-1 col-xs-2">
				<ul class="links list-unstyled">
					<li><a>Транспорт</a></li>
					<li><a>Для вечеринок и мероприятий</a></li>
					<li><a>Одежда и украшения</a></li>
				</ul>
			</div>
			<div class="col-xs-offset-1 col-xs-2">
				<ul class="links list-unstyled">
					<li><a>Бытова электроника</a></li>
					<li><a>Фото, аудио, видео</a></li>
					<li><a>Инструменты для дома и сада</a></li>
				</ul>
			</div>
			<div class="social pull-right">
         <a class="fa fa-2x fa-facebook" href="/" target="_blank"> </a>
         <a class="fa fa-2x fa-instagram" href="/" target="_blank"> </a>
         <a class="fa fa-2x fa-vk" href="/" target="_blank"> </a>
      </div>
		</div>
        <div class="row">
            <div class="col-xs-offset-4 col-xs-5 col-md-offset-5 col-md-2">
                <div class="contacts">
                    <a class="fa fa-2x fa-phone" href="#" target="_blank"
                        data-container="body" data-toggle="popover" data-placement="top"
                        data-content="8-918-123-23-22" data-trigger="hover"> </a>
                    <a class="fa fa-2x fa-map-marker" href="/" target="_blank"
                        data-container="body" data-toggle="popover" data-placement="top"
                        data-content="Краснодар" data-trigger="hover"> </a>
                    <a class="fa fa-2x fa-envelope-o" href="/" target="_blank"
                        data-container="body" data-toggle="popover" data-placement="top"
                        data-content="mail@mail.ru" data-trigger="hover"> </a>
                </div>
            </div>
        </div>
	</div>
</footer>

</body>
</html>