<!DOCTYPE html>
<html  xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php echo (!empty($title)?$title . ' - ':'') . $config['title']->value?></title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?php echo !empty($description)?$description:$config['meta_desc']->value?>" />
	<meta name="keywords" content="<?php echo !empty($keywords)?$keywords:$config['meta_keywords']->value?>" />

<!-- Подключение галереи fancybox -->	
	<!-- Add jQuery library -->
	<script type="text/javascript" src="/js/jquery-1.11.2.min.js"></script>
	<!-- Add fancyBox gallery for images--> 
	<link rel="stylesheet" href="/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	<script type="text/javascript" src="/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
	
	<!-- Optionally add helpers - button, thumbnail and/or media -->
	<link rel="stylesheet" href="/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
	<script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
	
	<link rel="stylesheet" href="/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
	<script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/additional-methods.min.js"></script> 
	
	<link rel="icon" href="/img/favicon.ico" type="image/x-icon"> 
	<link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
	
  <link rel="stylesheet" href="/css/bootstrap.min.css" media="screen">
	<link rel="stylesheet" href="/css/font-awesome-4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="/css/style.css">
	<script src="/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/js/script.js"></script>

</head>
<body>
	<?php
	   session_start();
	   if(isset($_SESSION['cart']))
	       $cart_len = count($_SESSION['cart']);
	   else
	       $cart_len = 0;
	?>
  <!--Горизонтальное меню  --> 
	<div class="main-header">
    	<nav role="navigation" class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="col-md-offset-3 col-md-2 col-xs-4">
                        <div class="navbar-header">
                            <div class="social hidden-xs">
                                <a class="fa fa-2x fa-facebook" href="/" target="_blank"> </a>
                                <a class="fa fa-2x fa-instagram" href="/" target="_blank"> </a>
                                <a class="fa fa-2x fa-vk" href="/" target="_blank"> </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-xs-4">
                        <a href="/" >
                            <img class="img-responsive" title="Скупердяй" alt="Скупердяй" src="/img/logo.png">
                        </a>
                    </div>
                    <div class="col-md-2 col-xs-4">
                        <div class="contacts pull-right hidden-xs">
                            <a class="fa fa-2x fa-phone" href="#" target="_blank"
                            data-container="body" data-toggle="popover" data-placement="top"
                            data-content="8-918-123-23-22"
                            data-trigger="hover"> </a>
                            <a class="fa fa-2x fa-map-marker" href="/" target="_blank"
                            data-container="body" data-toggle="popover" data-placement="top"
                            data-content="Краснодар"
                            data-trigger="hover"> </a>
                            <a class="fa fa-2x fa-envelope-o" href="/" target="_blank"
                            data-container="body" data-toggle="popover" data-placement="top"
                            data-content="mail@mail.ru"
                            data-trigger="hover"> </a>
                        </div>
                    </div>
                </div>
            </nav>
	   <div class="container">
            <div class="row">
                <div class="centered-nav">
                    <ul class="nav nav-pills">
                        <li><a href="/">Спорт и отдых</a></li>
                        <li><a href="/">Для детей</a></li>
                        <li><a href="/"> Для свадеб</a></li>
                        <li class="hidden-xs"> <a href="/">Транспорт</a></li>
                        <li class="hidden-xs"><a href="/">Для вечеринок и мероприятий</a></li>
                        <li class="visible-md-inline-block visible-lg-inline-block"><a href="/">Одежда и украшения</a></li>
                        <li class="dropdown" role="presentation">
                            <a aria-expanded="false" role="button" href="#" data-toggle="dropdown" class="dropdown-toggle more">
                                Ещё <span class="caret">
                            </a>
                            <ul role="menu" class="dropdown-menu dropdown-menu-right">
                                <li class="visible-xs-block"><a href="/">Транспорт</a></li>
                                <li class="visible-xs-block"><a href="/">Для вечеринок и мероприятий</a></li>
                                <li class="hidden-md hidden-lg"><a href="/">Одежда и украшения</a></li>
                                <li class="hidden-lg"><a href="/">Бытовая электроника</a></li>
                                <li><a href="/">Фото, Аудио, Видео оборудование</a></li>
                                <li><a href="/">Инструменты для дома и сада</a></li>
                            </ul>
                        </li>
                        <li role="presentation">
                            <a aria-expanded="false" role="button" href="#" class="cart">
                                <i class="fa fa-shopping-cart fa-lg"></i>Корзина <span class="badge cart-badge">0</span>
                           </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Корзина</h4>
        </div>
            <form class="form-horizontal" id = "cart_form">
                <div class="modal-body"> 
                    <div class="form-group">
                        <label for="inputName" class="col-xs-2 control-label">ФИО:</label>
                        <div class="col-xs-10">
                          <input type="text" class="form-control" name = "client_name" id="inputName" placeholder="Заказчик" required="required">
                        </div>
                  </div>
                  <div class="form-group">
                      <label for="inputAddress" class="col-xs-2 control-label">Адрес:</label>
                      <div class="col-xs-10">
                        <input type="text" class="form-control" name = "address" id="inputAddress" placeholder="Адрес доставки" required="required">
                      </div>
                  </div>
                  <div class="form-group">
                     <div class="col-xs-offset-2 col-xs-10 cart_items">
                          <table class = "cart_list table-striped" >
                        
                          </table>
                      </div>
                  </div>
                    <div class="col-xs-offset-7 col-xs-4">
                        На сумму: <span class = "price cart_summ"></span>р.
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-primary set_order" data-dismiss="modal">Заказать</button>
               </div>
            </form>
    </div>
  </div>
</div>