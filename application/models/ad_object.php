<?php 
class AdObject{
    public $item_id = 0;
    public $category_id = 0;
    public $article = 0;
    public $in_stock = 0;
    public $title = '';
    public $description = '';
    public $day_price = 0;
    public $week_price = 0;
    public $month_price = 0;
    public $pledge = 0;
    public $min_time = 0;
    public $link = '';
    public $meta_desc = '';
    public $meta_keywords = '';
}