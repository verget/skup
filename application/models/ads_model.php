<?php
class Ads_model extends CI_Model {
    public static $search_fields = [
                    'price1',
                    'price2',
                    'price',
                    'category_id',
		    		'article',
                    'description',
                    'min_time',
                    'pledge',
		    		'in_stock',
		    		'new',
		    		'sale'
    ];
    
    public function __construct(){
        $this->load->database();
    }
    
    public static function checkSearchFields($input){
        foreach( self::$search_fields as $field )
            if(! isset( $input[$field] ))
                $input[$field] = '';
        return $input;
    }
    
    public function fetch_ads($search = false, $limit = 20, $start = 0, $count = false){
    	if($parrent_cat = $search['category_id']){
    		unset( $search['category_id'] );
        	$child_cat_ids = array();
        	$child_cats = $this->get_child_categories($parrent_cat);
        	$where = "(categories.id = '".$parrent_cat."' ";
        	if ($child_cats){
        	    foreach ($child_cats as $cat){
        	        $child_cat_ids[]=$cat['id'];
        	    }
        	    $where .= "OR categories.id IN ('".implode("','",$child_cat_ids)."'))";
        	}else
        	    $where .= ")";
        	 
        	if(! empty( $search )){
        	    if(!empty($search['price_type'])){
        	        if(! empty( $search['price_from'] )){
        	            if(! empty( $search['price_to'] )){
        	                $where .= " AND (".$search['price_type']." >= '" . $search['price_from'] ."' AND ".$search['price_type']." <= '". $search['price_to'] ."')";
        	            }else{
        	                $where .= " AND (".$search['price_type']." >= '" . $search['price_from'] ."')";
        	            }
        	            unset( $search['price_from'] );
        	            unset( $search['price_to'] );
        	            unset( $search['price_type'] );
        	        }
        	    }
        	    if (!empty($search)){
        	        foreach ($search as $key=>$value){
        	            if(!empty($value))
        	                $where .= " AND ".$key."= '".$value."'";
        	            unset( $key );
        	        }
        	    }
        	}
        	
    	}
        	$this->db->select( [
        	'items.*',
        	'categories.category_title',
        	'items_category.*',
        	'prices.*',
        	'items_images.image_url',
        	] );
        	
        	$this->db->from( 'items' );
        	
        	if (isset($where))
        	   $this->db->where($where);
        	$this->db->query('SET SQL_BIG_SELECTS=1');
        	
        	$this->db->join( 'items_category', 'items_category.item_id = items.id', 'LEFT' );
        	$this->db->join( 'categories', 'categories.id = items_category.category_id' );
        	$this->db->join( 'prices', 'prices.item_id = items.id', 'LEFT' );
        	$this->db->join( 'items_images', 'items_images.item_id = items.id', 'LEFT' );
        	
        	
        	$this->db->group_by( 'items.id' );
        	
        	if($count){
        	    $this->db->distinct();
        	    $query = $this->db->get();
        	    
        	    if($query->num_rows() > 0){
        	        $data = [];
        	        foreach( $query->result() as $row ) {
        	            $data[] = $row;
        	        }
        	        return count($data);
        	    }
        	}
        	$this->db->limit( $limit, $start );
        	$query = $this->db->get();
        	//var_dump($this->db->last_query());
        	if($query->num_rows() > 0){
        		$data = [];
        		foreach( $query->result() as $row ) {
        			$data[] = $row;
        		}
        		return $data;
        	}
        return false;
    }
    
    public function get_category_by_name($name){
    	$query = $this->db->query("SELECT categories.* FROM categories WHERE category_name = '".$name."'");
    	if ($query->num_rows() > 0)
    		return $query->row();
    	else 
    		return false;
    }
    public function get_category_by_id($id){
    	$query = $this->db->query("SELECT categories.* FROM categories WHERE id = '".$id."'");
    	if ($query->num_rows() > 0)
    		return $query->row();
    	else
    		return false;
    }
    public function get_child_categories($parrent_id){
    	$this->db->select( [
    			'categories.*'
    			] );
    	$this->db->from( 'categories' );
    	$this->db->where( 'categories.parrent_id', $parrent_id );
    	$res = $this->db->get();
    	if($res->num_rows() > 0){
    		return $res->result_array();
      }else 
        return false;
    }
    
    public function fetch_one_ads($ads_id){ //@todo
        $this->db->limit( 1 );
        $this->db->select( [
                        'items.*',
                		'categories.category_title',
                		'items_category.*',
			            'items_images.image_url',
                        'prices.*',
        ] );
        $this->db->from( 'items' );
        
        $this->db->join( 'items_category', 'items_category.item_id = items.id', 'LEFT' );
        $this->db->join( 'categories', 'categories.id = items_category.category_id', 'LEFT' );
        $this->db->join( 'items_images', 'items_images.item_id = items.id', 'LEFT' );
        $this->db->join( 'prices', 'prices.item_id = items.id', 'LEFT' );
      
        $this->db->where( 'items.id = "' . $ads_id . '"' );
        
        $res = $this->db->get();
        if($res->num_rows() > 0){
            $tmp = $res->result()[0];
            $tmp->images = $this->getAdsImages( $tmp->item_id );
            return $tmp;
        }
        return false;
    }
    
    public function storeAds($params, $category = 0, $prices = 0, $ad_id = 0){
        if(empty( $params ))
            return false;
        if($ad_id){ 
            $category = ['category_id' => $category['category_id']]; //@todo few cats
            $prices['item_id'] = $ad_id;
            $alpha = $this->db->update( 'items', $params, ['id' => $ad_id], 1 );
            $betta =  $this->db->update( 'items_category', $category, ['item_id' => $ad_id], 1 );
            $gamma = $this->db->update( 'prices', $prices, ['item_id' => $ad_id], 1 );
            return ($alpha && $betta && $gamma);   
        }else{
            if($this->db->insert( 'items', $params )){
                $ad_id = $this->db->insert_id();
                $category = ['item_id' => $ad_id, 'category_id' => $category['category_id']]; 
                foreach ($prices as $key=>$value){
                    if(!$value)
                        $prices[$key] = 0;
                }
                $prices['item_id'] = $ad_id;             
                if ($this->db->insert( 'items_category', $category ) && $this->db->insert( 'prices', $prices ))
                    return true;
            } 
            return false;
            return $this->db->insert( 'items', $params );
        }
    }
    
    public function removeAds( $ad_id ){
        $images = $this->getAdsImages($ad_id);
        if( !empty($images) ){
            foreach ($images as $img)
                @unlink(APPPATH . '../images/' . $img->image_url);
            $this->db->delete('items_images', 'item_id = ' . $ad_id);
        }
        $this->db->delete('items', 'id = ' . $ad_id);
        $this->db->delete('items_category', 'item_id = ' . $ad_id);
        $this->db->delete('prices', 'item_id = ' . $ad_id);
        return true;
    }
    
    public function storeAdsImage($fileName, $ad_id){
        $this->db->insert( 'items_images', [
                        'item_id' => $ad_id,
                        'image_url' => $fileName 
        ] );
        return $this->db->insert_id();
    }
    
    public function removeAdsImage($img_id){
        $this->db->select( 'image_url' );
        $this->db->from( 'items_images' );
        $this->db->where( 'id = ' . $img_id );
        $res = $this->db->get();
        if($res->num_rows() <= 0)
            return false;
        
        $img = $res->result()[0];
        @unlink( APPPATH . '../images/' . $fileName );
        
        return $this->db->delete( 'items_images', 'id = ' . $img_id );
    }
    
    public function uploadAdsImages($ad_id){
        $res = [];
        if(! empty( $_FILES )){
            for($i = 0; $i < count( $_FILES['photos']['name'] ); $i ++){
                $fileExt = pathinfo( $_FILES['photos']['name'][$i], PATHINFO_EXTENSION );
                $fileName = $ad_id . '_' . md5( $ad_id . $_FILES['photos']['tmp_name'][$i] ) . '.' . $fileExt;
                if(move_uploaded_file( $_FILES['photos']['tmp_name'][$i], APPPATH . '../images/' . $fileName )){
                    $newId = $this->storeAdsImage( $fileName, $ad_id );
                    $res[] = [
                                    'file_name' => $fileName,
                                    'image_id' => $newId 
                    ];
                }
            }
        }
        return $res;
    }
    
    public function getAdsImages($ads_id){
        $this->db->from( 'items_images' );
        $this->db->where( 'item_id', $ads_id );
        
        $query = $this->db->get();
        if($query->num_rows() > 0){
            $data = [];
            foreach( $query->result() as $row )
                $data[] = $row;
            return $data;
        }
        return false;
    }
    
    public function getAvailCategory($appId = false, $appName = false){
    	$this->db->select( 'categories.category_title, id' );
    	$this->db->from( 'categories' );
    	//$this->db->join( 'items_category', 'items_category.id = category_id', 'LEFT' );
    	$this->db->group_by( 'id' );
    
    	$query = $this->db->get();
    	if($query->num_rows() > 0){
    		$data = [];
    		if(! empty( $appId ) || ! empty( $appName )){
    			$tmp = new stdClass();
    			$tmp->id = $appId;
    			$tmp->category_title = $appName;
    			$data[] = $tmp;
    		}
    		foreach( $query->result() as $row )
    			if($row->category_title)
    				$data[] = $row;
    
    			return $data;
    	}
    	return false;
    }

    public function getItems($cart){
        $array = array();
        foreach($cart as $item){
            $this->db->select( '*');
            $this->db->from( 'items' );
            $this->db->where('id', $item);
            $query = $this->db->get();
            if($query->num_rows() > 0){
                foreach( $query->result() as $row ) {
                    $array[] = $row;
                }
            }
        }
        return $array;
    }
    
     public function setOrder($name, $address, $items) {
         $this->db->insert( 'orders', [
                         'client_name' => $name,
                         'client_address' => $address,
                         'items' => implode(",", $items),
         ] );
         return $this->db->insert_id();
     }
     
     public function getOrders() {
         $array = array();
         $this->db->select( '*');
         $this->db->from( 'orders' );
         $query = $this->db->get();
         if($query->num_rows() > 0){
             foreach( $query->result() as $row ) {
                 $array[] = $row;
             }
         }
         return $array;
     }

    
    public function getCategoryList($appId = false, $appName = false){
        $this->db->from( 'categories' );
        
        $query = $this->db->get();
        if($query->num_rows() > 0){
            $data = [];
            if(! empty( $appId ) || ! empty( $appName )){
                $tmp = new stdClass();
                $tmp->id = $appId;
                $tmp->category_title = $appName;
                $data[] = $tmp;
            }
            foreach( $query->result() as $row )
                if($row->category_title)
                 $data[] = $row;
            return $data;
        }
        return false;
    }
    
    
}
